# Introduction

  Pour le jeu de Domino en JAVA on aura
  besoin de tous les fichiers et images
  nécessaires pour le bon fonctionnement
  du jeu.
  Les dossiers sont les suivants:
  /UE_PROJET/src
  /UE_PROJET/images/Dominos
  /UE_PROJET/images/Icone

# Compilation

  Afin d'éxecuter le jeu, il suffit de se rendre
  dans le dossier /src et d'éffectuer les commandes suivantes:

  1/- javac *.java

  2/- java Main.java

  Une fois la compilation terminée, une fenêtre s'ouvrira,
  et en cliquant sur la petite fenêtre domino,
  vous aurez le choix du nombre de joueurs àdéterminer d'abord,
  puis d'entrer un nom de joueurs proportionnelle au Nombre
  de joueurs indiqué précedemment.

  Et vous pourrez enfin jouer (à quelques problèmes d'affichages près).
