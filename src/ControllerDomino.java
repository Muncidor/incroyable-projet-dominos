import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Cette classe est responsable des interactions 
 * avec l'utilisateur pour le jeu du domino.
 */
public class ControllerDomino {

    /** Reprise de la vue principale */
    private ViewDomino view;
    /** Prise en charge du modèle correspondant */
    protected Domino model;
    /** Utilisation d'un listener pour 
     * les évènement de la souris
     */
    private ControllerPiece souris;
    /** Listener pour le bouton 'Piocher!' */
    private Controllerpioche pioche;

    public ControllerDomino(ViewDomino v, Domino m) {
        view = v;
        model = m;
        souris = new ControllerPiece();
        pioche = new Controllerpioche();
    }
     /** 
     * Getters
     */
    public ControllerPiece getControllerPiece() {
        return souris;
    }

    public Controllerpioche getControllerpioche() {
        return pioche;
    }
    /**
     * Classes internes pour séparer les fontionnalités du listener. 
     */
    private class Controllerpioche implements ActionListener {
        /**
         * Cette fonction permet de piocher un domino,
         * si l'utilisateur pioche.
         * Elle surveille aussi la disponibilité des 
         * pièces dans la pioche.
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();
            if (!model.domino.isEmpty()) {
                if(command.equals("pioche")){
                    view.addPieceToPlayer();
                }
            }
            if (model.domino.isEmpty()) {
                view.setpiocheDisable();
            }
        }
    }

    private class ControllerPiece implements MouseListener, MouseMotionListener {
        /**Localisation des coordonnées de la souris en temps réel */
        private Point point;

        public ControllerPiece() {
            point = new Point(0, 0);
        }

        /**
         * Permet les mouvements de souris afin 
         * d'organiser son jeu, si besoin.
         */
        @Override
        public void mouseMoved(MouseEvent e) {
            point = e.getPoint();
            
        }
        
        @Override
        public void mouseDragged(MouseEvent e) {
            int x = e.getX() + e.getComponent().getX() - (int) point.getX();
            int y = e.getY() + e.getComponent().getY() - (int) point.getY();
            JLabel l = (JLabel) e.getSource();
            l.setLocation(x, y);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }
        /**
         * Met un domino sur le plateau, 
         * si jamais elle est compatible.
         */
        @Override
        public void mouseClicked(MouseEvent e){
            point = e.getPoint();
            JLabel l = (JLabel) e.getSource();
            if(!l.getName().equals("")){
                view.setOnPlateau(l);
            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }
}
