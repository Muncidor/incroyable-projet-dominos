/**
 * La classe Piece est globale au jeu. 
 * Elle represente toutes les Pieces du jeu 
 * en utilisant l'héritage.
 */
public abstract class Piece{
    /**
     * Coordonnées du point
     */
    protected int x;
    protected int y;
    /**
     * 
     * @param x coordonnées des abscisses
     * @param y coordonnées des ordonnees
     */
    public Piece(int x, int y){
        this.x=x;
        this.y=y;
    }
    public Piece(){
        this.x=0;
        this.y=0;
    }
    /** 
     * Getters et Setters 
     */
    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public void setX(int x){
        this.x=x;
    }
 
    public void setY(int y){
        this.y=y;
    }
}