/**
 * La classe Player est globale au jeu. 
 * Elle represente tous les joueurs du jeu. 
 * 
 */

import java.util.*;

public class Player<E extends Piece>{

    /** Nom */
    protected final String name;

    /** Identifiant */
    protected final int id;

    /** Tour */
    protected boolean turn = false;

    /** Pieces dont le joueur possede */
    protected LinkedList<E> pieces;

    /** Incrementation de l'identifiant */
    protected static int i = 0;

    /**
     * Creation d un joeur. 
     * @param n nom du joeur. 
     */
    public Player(String n){
        name = n;
        id = i;
        i++;
        pieces = new LinkedList<>();
    }

    /**
     * Permet d ajouter une piece, a un joueur.
     * @param p Piece atribue de facon aleatoire.
     */
    public void addPieceToPlayer(E p){
        pieces.add(p);
    }

    /**
     * Permet de retirer une piece, a un joueur.
     * @param p pièce retiré par le joueur.
     * @exception RuntimeExeption c'est un cas qui ne devrait pas arriver.
     */
    public void removePieceToPlayer(E p){
       pieces.remove(p);
    }

    /**
     * 
     * @return des informations sur le joueur.
     */
    public String getDescription(){
        String isTurn = turn ? " c'est son tour": "";
        String s = "Joueur: "+name + isTurn +"\n";
        for (E p : pieces) {
            s+=p.toString()+"\n";
        }
        return s;
    }

    /**
     * Affiche les dominos du joueur.
     */
    public void listeCarte(){
        for(int k = 0; k < this.getSize(); k++)
            System.out.println(k+" - "+this.getPieces().get(k));
        System.out.println();
    }

    public String toString() {
        return name;
    }
    
    /** 
     * Getters et Setters 
     */
    public LinkedList<E> getPieces(){
        return pieces;
    }

    public String getName(){
        return name;
    }

    public int getId(){
        return id;
    }
        
    public boolean getTurn(){
        return turn;
    }
        
    public void setTurn(boolean b){
        turn=b;
    }

    public int getSize(){
        return pieces.size();
    }
        
}