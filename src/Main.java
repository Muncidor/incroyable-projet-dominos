import javax.swing.*;
import java.util.*;
/**
 * Classe principale qui lance l'application.
 */
public class Main implements Runnable {
    @Override
    public void run(){
        ViewMain w = new ViewMain();
        w.setVisible(true);
    }
    /**
     * Fonction qui permet de lancer le jeu en interface graphique.
     */
    public static void launcher(){
        
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);
        System.out.println("Appuyez sur la touche entrée pour jouer :  ");
    
        int choix = sc.nextInt();
        
		if(choix == 1){
            SwingUtilities.invokeLater(new Main());      
		}else{
            System.out.println("Il faut entrer 1 !!!");
            launcher();
        }
    }
    public static void main(String[] args) {
        launcher();
        
    }
}