import java.awt.event.*;
/**
 * Classe correspondant au contrôleur principal 
 * du menu de démarrage de l'interface graphique.
 */
public class ControllerMain implements ActionListener{
    /** JFrame principale tout au long 
     * de l'éxecution du jeu.
     */
    private ViewMain view;

    public ControllerMain(ViewMain v){
        view=v;
    }
    /**
     * Listener pour le choix du jeu a demarrer.
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e){   
        String c = e.getActionCommand();  
        if(c.equals("domino")){
            new ViewDomino(view);
        }
        view.getPanel().setVisible(false);
    }
    
}